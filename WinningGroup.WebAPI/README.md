﻿# Winning Group Test
POST /api/products​: Saves a new product to the /assets/products/ directory as a
JSON file, named by its product ID.

GET /assets/products/<id>.json​: Gets the ​static
​
json file for the product with
the given ID.

##Sample POST body request:
{
    "Id": "T1902",
    "Name": "Terminator 1 CD",
    "Price": 19.8
}


##Explanation about the projects
1- WinningGroup.Tests
All the unit tests are in this project

2-WinningGroup.Util
I put reusable functions here to be able to use them in other projects (Still there can be more function moved to here. Because of time that I want spend on this project I stop at this point)

3-WinningGroup.WebAPI
This is the startup project that serves the HTTP POST requests and saves them on assets/products folder
The web server is setup at the startup so that it serves the files in assets/products as static files and will return them upon getting a GET request for them.


