﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WinningGroup.WebAPI.Model
{
    [AssetFolder(Constants.PRODUCT_FILES_PATH)]
    public class Product : IModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Product;
            return other!=null && 
                Id==other.Id &&
                Name == other.Name &&
                Price == other.Price;
        }
    }
}
