﻿using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WinningGroup.Util;

namespace WinningGroup.WebAPI.Helpers
{
    public static class ExtensionMethods
    {
        #region Serialisation
        public static string ToJson(this object subject)
        {
            return JsonConvert.SerializeObject(subject);
        }

        public static T ToObject<T>(this string jsonContent)
        {
            return JsonConvert.DeserializeObject<T>(jsonContent);
        }
        #endregion

        #region File handling
        public static void WriteToFile(this string content, string fullPath)
        {
            File.WriteAllText(fullPath, content);
        }

        public static void MakeSureFolderExists(this string fileFullPath)
        {
            var info = new FileInfo(fileFullPath);
            Directory.CreateDirectory(info.DirectoryName);
        }

        public static bool FileExists(this string fullPath)
        {
            return File.Exists(fullPath);
        }
        #endregion

        #region Model storage
        public static string MapToAbsoluteFileName(this IModel model, string webRootPath)
        {
            if (string.IsNullOrEmpty(model.Id))
            {
                throw new ArgumentException("Id field should not be empty.");
            }

            var folderName = model.GetType().GetAssetFolderName();
            return Path.Combine(webRootPath, folderName, $"{model.Id}.json");
        }

        public static string MapIdToAbsoluteFileName<T>(this string id, string webRootPath) 
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id parameter should not be empty.");
            }

            var folderName = typeof(T).GetAssetFolderName();
            return Path.Combine(webRootPath, folderName, $"{id}.json") ;
        }

        public static string MapIdToVirtualPath<T>(this string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id parameter should not be empty.");
            }

            var folderName = typeof(T).GetAssetFolderName();
            return Path.Combine(folderName, $"{id}.json");
        }

        public static string GetAssetFolderName(this Type type)
        {
            return type.GetAttributeValue((AssetFolderAttribute afa) => afa.FolderName);
        }
        #endregion
    }
}
