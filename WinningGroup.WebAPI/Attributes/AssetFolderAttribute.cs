﻿using System;

namespace WinningGroup.WebAPI
{
    [AttributeUsage(AttributeTargets.Class)]
    class AssetFolderAttribute : Attribute
    {
        private string _folderName;

        public AssetFolderAttribute(string folderName)
        {
            _folderName = folderName;
        }

        public virtual string FolderName
        {
            get { return _folderName; }
        }
    }
}