﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WinningGroup.WebAPI.Helpers;
using WinningGroup.WebAPI.Interface;
using WinningGroup.WebAPI.Model;

namespace WinningGroup.WebAPI.Storage
{
    public class FileSystemStorage<T> : IStorageRetrieval<T> where T : IModel
    {
        private string _webRoot;
        public FileSystemStorage(string webRoot)
        {
            _webRoot = webRoot;
        }

        public Task<string> GetVirtualAddress(string id)
        {
            return Task.Run(() => {
                var fullPath = id.MapIdToAbsoluteFileName<T>(_webRoot);

                if (fullPath.FileExists())
                {
                    return id.MapIdToVirtualPath<T>();
                }
                else
                {
                    return string.Empty;
                }
            });
        }

        public Task<bool> StoreAsync(T model)
        {
            return Task.Run(() => {
                try
                {
                    var fullPath = model.MapToAbsoluteFileName(_webRoot);

                    var info = new FileInfo(fullPath);
                    Directory.CreateDirectory(info.DirectoryName);

                    model.ToJson().WriteToFile(fullPath);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            });
        }
    }
}
