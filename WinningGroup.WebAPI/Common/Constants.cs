﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WinningGroup.WebAPI
{
    public class Constants
    {
        public const string PRODUCT_FILES_PATH = "assets\\products";

        public const string CONTENT_JSON = "application/json";
    }
}
