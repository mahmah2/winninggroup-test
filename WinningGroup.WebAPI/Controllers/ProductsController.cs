﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WinningGroup.WebAPI.Interface;
using WinningGroup.WebAPI.Model;

namespace WinningGroup.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IStorageRetrieval<Product> _storageRetrieval;

        public ProductsController(IStorageRetrieval<Product> storageRetrieval)
        {
            _storageRetrieval = storageRetrieval;
        }

        [HttpPost]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Post([FromBody] Product product)
        {
            try
            {
                await _storageRetrieval.StoreAsync(product);

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }


    }
}
