﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WinningGroup.WebAPI.Interface
{
    public interface IStorageRetrieval<T>
    {
        Task<bool> StoreAsync(T model);
        Task<string> GetVirtualAddress(string id);
    }
}
