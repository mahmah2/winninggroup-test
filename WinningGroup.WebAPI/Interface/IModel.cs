﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WinningGroup.WebAPI
{
    public interface IModel
    {
        string Id { get; set; }
    }
}
