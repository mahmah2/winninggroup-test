using System;
using System.IO;
using WinningGroup.WebAPI.Helpers;
using WinningGroup.WebAPI.Model;
using WinningGroup.WebAPI.Storage;
using Xunit;

namespace WinningGroup.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void TestModelAttributes()
        {
            var assetFolderName = ExtensionMethods.GetAssetFolderName(typeof(Product));

            Assert.Equal("assets\\products", assetFolderName);
        }

        [Fact]
        public async void TestStorageSystemInstance()
        {
            var rootFolder = "c:\\temp";

            var storage = new FileSystemStorage<Product>(rootFolder);

            Directory.CreateDirectory(Path.Combine(rootFolder,
                typeof(Product).GetAssetFolderName()));

            var originalProduct = new Product() { Id = "", Name = "toy", Price = 19.01M };

            var successStorage =  await storage.StoreAsync(originalProduct);

            Assert.False(successStorage, "Empty Id should not be stored.");

            originalProduct.Id = "toy1990";

            successStorage  = await storage.StoreAsync(originalProduct);

            Assert.True(successStorage, "Storage successful test.");

            var virtualAddress = await storage.GetVirtualAddress(originalProduct.Id);

            var addressToLoad = Path.Combine(rootFolder, virtualAddress);

            var content = File.ReadAllText(addressToLoad);

            var loadedProduct = content.ToObject<Product>();

            var loadSuccess = originalProduct.Equals(loadedProduct);

            Assert.True(loadSuccess);
        }
    }
}
